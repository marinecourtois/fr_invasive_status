# **Datasets and R Source Code for the Manuscript**  
**_The functional response predicts invasiveness but not trophic impacts of invasive species_**

## **Overview**  
This repository contains the datasets and R scripts used in the analysis for the above-mentioned manuscript. The study explores the relationship between functional response parameters and the invasive potential of species, highlighting the importance of the species' origin (native vs. invasive range) and the influence of temperature on trophic impacts.

---

## **Glossary**  
- **Functional Response Parameters**:  
  - **a**: Space clearance rate.  
  - **h**: Handling time.  
  - **a/h**: Functional Response Ratio (FRR).  

---

## **Datasets**  

### **1. Phylogenetic Tree**: `fishtree_invasive.phy`  
A phylogenetic tree comprising all species listed in the "predator_scientific_name" column of the dataset `FR_data_invasive.csv`.  

---

### **2. Functional Response Database**: `FR_data_invasive.csv`  
A comprehensive dataset derived from the FoRAGE database (DOI: [10.5063/F17H1GTQ](https://doi.org/10.5063/F17H1GTQ)), containing functional response parameters for **2083 predator-prey and parasitoid-host interactions**. The data includes parameters specific to habitat, predator and prey traits, body mass, temperature, and experimental conditions.  

In this study, only **freshwater fish** data were extracted. The dataset studied contains **353 estimates** of functional response parameters from **51 species** and **50 studies**. 

#### **Column Descriptions**:  
For a detailed explanation of each variable, see the "Variables Description" section below.

---

## **R Scripts**  

### **1. Phylogenetic Tree Creation**: `phylogeny_freshwater_fish_invasive.R`  
This script generates the phylogenetic tree file (`fishtree_invasive.phy`) using species data from `FR_data_invasive.csv`.

---

### **2. Manuscript Analysis**: `Script_manuscrit_invasive_status.R`  
This script performs the following analyses:  
- **MCMCglmm Analyses**: Bayesian generalized linear mixed-effects models applied to the dataset `FR_data_invasive.csv`.  
- **Graph Generation**: Code to produce all figures used in the manuscript.  

---

## **Variable Descriptions for `FR_data_invasive.csv`**  

The dataset contains variables detailing predator-prey interactions, environmental factors, and functional response parameters. Variables description in column order:  

- **`data_set`**: Unique identifier for each data line.  
- **`source`**: Publication from which the functional response data was obtained.  
- **`predation_or_parasitism`**: Type of interaction (`Pred` = predator-prey, `Par` = parasitoid-host).  
- **`dim`**: Dimensions in which the functional response was produced.  
- **`field?`**: Indicates whether the functional response data was obtained from field observations or laboratory experiments.  
- **`habitat`**: Specifies the interaction habitat as aquatic, terrestrial, or mixed.  
- **`fresh_or_marine`**: Salinity of the water in which the functional response was performed, either freshwater or marine.  
- **`predator_cellularity`**: Cellularity of the consumer, either metazoan or unicellular.  
- **`Vert_invert`**: Specifies if the consumer is a vertebrate, invertebrate, or protozoan.  
- **`major_grouping_1`**: Broad taxonomic group of the consumer.  
- **`major_grouping_2`**: Narrower taxonomic group of the consumer.  
- **`predator_scientific_name`**: Scientific name of the predator.  
- **`predator_max_length_cm`**: Average maximum fish length in centimeters (source: FishBase).  
- **`predator_length_type`**: Type of length measurement used (e.g., Total Length (TL), Standard Length (SL), Fork Length (FL)), (source: FishBase).  
- **`temp_type`**: Thermal category of the predator: `eurytherm` (broad temperature tolerance) or `stenotherm` (narrow temperature tolerance).  
- **`dispersal_type`**: Dispersal capacity classified into two categories: `migrant` or `non-migrant`.  
- **`location_type`**: Predator’s location in the water column: pelagic, benthopelagic, or demersal.  
- **`species_type`**: Classification of the species as `invasive` or `non-invasive`.  
- **`nb_countries_introduced`**: Number of countries where the species has been introduced.  
- **`sampling_area`**: Indicates whether the predator was sampled in its native (`native`) or invasive (`invasive`) range.  
- **`evidence_of_impact`**: Whether (`yes`/`no`) the species has documented impacts in at least one introduced country.  
- **`predator_type`**: Additional details about the predator (e.g., age, sex).  
- **`trophic_level_predator`**: Trophic level of the predator.  
- **`prey_cellularity`**: Indicates whether the prey is multicellular (`metazoan`), unicellular, or non-living food.  
- **`vert_invert`**: Specifies if the resource is a vertebrate, invertebrate, or protozoan.  
- **`major_grouping_1`**: Broad taxonomic group of the resource.  
- **`major_grouping_2`**: Narrower taxonomic group of the resource.  
- **`prey`**: Common name or other descriptor of resource.  
- **`prey_scientific_name`**: Scientific name of the prey.  
- **`prey_type`**: Additional details about resource (e.g., age, sex).  
- **`trophic_level_prey`**: Trophic level of the prey.  
- **`data_type`**: Data format as presented in the original source: mean (average foraging rates for given densities) or raw data (data points each corresponding to a single foraging rate).  
- **`prey_replaced?`**: Indicates whether resource items were replaced during the experiment (`Y`/`N`).  
- **`hours_starved`**: Duration in hours the predator was starved prior to the experiment.  
- **`temp_celsius`**: Temperature in which the experiment was conducted (for ectotherms) or body temperature of endotherms.  
- **`interference`**: `Y` indicates that the dataset is part of a set of similar functional responses with varying numbers of consumers. 
- **`pred_per_arena`**: Number of consumers in the experimental arena.  
- **`2D_arena_size_cm2_bottom_SA`**: Bottom surface area (cm²) of the experimental arena for 2D/2.5D interactions.  
- **`3D_arena_size_cm3`**: Volume (cm³) of the experimental arena for 3D/2.5D interactions.  
- **`predator_mass_mg`**: Estimated wet mass of the predator (mg).  
- **`predator_mass_source_code`**: Method used to determine body mass of the consumer. “Original” indicates measurements given in the original publication, “Alternate” indicate measurements given in an outside source. “Species”, “Genus”, “Family”, etc. indicate the most specific taxonomic level of the measurement. “% adult” indicates the measurement was obtained from an estimated juvenile:adult body size ratio. “Length” indicates a body length measurement, “Mass” indicates a body mass measurement, “Volume” indicates a body volume measurement.  
- **`predator_mass_identifier`**:  Indicates the taxonomic level of the length-mass regression used to convert body length to body mass.  
- **`prey_mass_mg`**: Estimated wet mass of the resource (mg).  
- **`prey_mass_source_code`**: Method used to determine mass of the resource. “Original” indicates measurements given in the original publication, “Alternate” indicate measurements given in an outside source. “Species”, “Genus”, “Order”, etc. indicate the most specific taxonomic level of the measurement. “% adult” indicates the measurement was obtained from an estimated juvenile:adult body size ratio. “Length” indicates a body length measurement, “Mass” indicates a body mass measurement, “Volume” indicates a body volume measurement.
- **`prey_mass_identifier`**: Indicates the taxonomic level of the length-mass regression used to convert body length to body mass.  
- **`units_of_a`**: Units of the estimated space clearance rate (`a`).  
- **`mean_R2_of_fits`**: Mean R² of the bootstrapped fits.  
- **`fitted_a_median_of_BS_samples`**: Median estimate of space clearance rate (`a`) from bootstrapped fits.  
- **`CI_a_low`**: Lower 95% confidence interval for `a`.  
- **`CI_a_hi`**: Upper 95% confidence interval for `a`.  
- **`fitted_h_day`**: Median estimate of handling time (`h`, in days) from bootstrapped fits.  
- **`CI_h_low`**: Lower 95% confidence interval for `h`.  
- **`CI_h_hi`**: Upper 95% confidence interval for `h`.  






